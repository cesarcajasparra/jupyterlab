FROM python:3.8.5-slim

RUN mkdir /app
COPY /jupyter /app
WORKDIR /app
RUN pip install jupyterlab
